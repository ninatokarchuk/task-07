package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private Connection getConnection() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("app.properties"));
            return DriverManager.getConnection(properties.getProperty("connection.url"));
        } catch (SQLException | IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(DBConstants.GET_ALL_USERS)) {
            while (rs.next()) {
                users.add(mapUser(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBException in findAllUsers() method", e);
        }
        return users;
    }

    private User mapUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt(DBConstants.F_ID));
        user.setLogin(rs.getString(DBConstants.F_USER_LOGIN));
        return user;
    }

    private Team mapTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt(DBConstants.F_ID));
        team.setName(rs.getString(DBConstants.F_TEAM_NAME));
        return team;
    }

    public void insertUser(User user) throws DBException {
        ResultSet generatedId = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DBConstants.INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, user.getLogin());
            preparedStatement.executeUpdate();
            generatedId = preparedStatement.getGeneratedKeys();
            if (generatedId.next()) {
                user.setId(generatedId.getInt(1));
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        } finally {
            close(generatedId);
        }
    }

    public void insertTeam(Team team) {
        ResultSet generatedId = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DBConstants.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
            generatedId = preparedStatement.getGeneratedKeys();
            if (generatedId.next()) {
                team.setId(generatedId.getInt(1));
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        } finally {
            close(generatedId);
        }
    }

    private void close(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try (Connection con = getConnection();
             PreparedStatement stmt = con.prepareStatement(DBConstants.GET_USER_BY_LOGIN, Statement.RETURN_GENERATED_KEYS)
        ) {
            stmt.setString(1, login);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    user = mapUser(rs);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBException in getUser() method", e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try (Connection con = getConnection();
             PreparedStatement stmt = con.prepareStatement(DBConstants.GET_TEAM_BY_NAME, Statement.RETURN_GENERATED_KEYS)
        ) {
            stmt.setString(1, name);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    team = mapTeam(rs);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBException in getTeam() method", e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(DBConstants.GET_ALL_TEAMS)) {
            while (rs.next()) {
                teams.add(mapTeam(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBException in findAllTeams() method", e);
        }
        return teams;
    }

    public void setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(DBConstants.SET_TEAMS_FOR_USER);

            for (Team team : teams) {
                stmt.setInt(1, user.getId());
                stmt.setInt(2, team.getId());
                stmt.executeUpdate();
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con);
            throw new DBException("DBException in insertTeam() method", e);
        } finally {
            close(stmt);
            close(con);
        }
    }

    private void rollback(Connection con) {
        try {
            con.rollback();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = getConnection();
             PreparedStatement stmt = con.prepareStatement(DBConstants.GET_USER_TEAMS)) {
            stmt.setInt(1, user.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                teams.add(mapTeam(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBException in findAllTeams() method", e);
        }
        return teams;
    }

    public void deleteTeam(Team team) throws DBException {
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(DBConstants.DELETE_TEAM_BY_NAME)) {
            ps.setString(1, team.getName());
            ps.executeUpdate();

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }
    }

    public void updateTeam(Team team) throws DBException {
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(DBConstants.UPDATE_TEAM)) {
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }
    }

    public void deleteUsers(User... users) throws DBException {
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(DBConstants.DELETE_USER_BY_LOGIN)) {
            for (User user : users) {
                ps.setString(1, user.getLogin());
                ps.executeUpdate();
            }
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }

    }

}
