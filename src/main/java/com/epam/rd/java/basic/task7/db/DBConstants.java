package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
    private DBConstants() {

    }

    //QUERIES
    public static final String GET_ALL_USERS = "SELECT * FROM users";
    public static final String GET_USER_BY_LOGIN = "SELECT * FROM users WHERE users.login=?";
    public static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT ,?)";
    public static final String GET_ALL_TEAMS = "SELECT * FROM teams";
    public static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT ,?)";
    public static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    public static final String GET_USER_TEAMS = "SELECT teams.id, teams.name FROM users_teams" +
            " JOIN users ON users_teams.user_id = users.id" +
            " JOIN teams ON users_teams.team_id = teams.id WHERE users.id =?";
    public static final String GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE teams.name=?";
    public static final String DELETE_TEAM_BY_NAME = "DELETE FROM teams WHERE name=?";
    public static final String UPDATE_TEAM = "UPDATE teams SET name =? WHERE id = ?";
    public static final String DELETE_USER_BY_LOGIN = "DELETE FROM users WHERE users.login=?";


    //FIELDS
    public static final String F_ID = "id";
    public static final String F_USER_LOGIN = "login";
    public static final String F_TEAM_NAME = "name";
}
